package main

import (
	"fmt"
	"gopl/ch1/join"
	"os"
	"strings"
	"time"
)

func main() {
	list := os.Args
	task1 := time.Now()
	join.Join1(list, " ")
	fmt.Println(time.Since(task1))

	task2 := time.Now()
	join.Join2(list, " ")
	fmt.Println(time.Since(task2))

	task3 := time.Now()
	strings.Join(list, " ")
	fmt.Println(time.Since(task3))
}

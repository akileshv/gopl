package join

func Join1(list []string, sepItem string) string {
	var s, sep string
	for i := 0; i < len(list); i++ {
		s += sep + list[i]
		sep = sepItem
	}
	return s
}

func Join2(list []string, sepItem string) string {
	var s, sep string
	for _, arg := range list {
		s += sep + arg
		sep = sepItem
	}
	return s
}
